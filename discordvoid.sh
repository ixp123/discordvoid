#!/bin/sh

git clone https://github.com/void-linux/void-packages

cd void-packages

./xbps-src binary-bootstrap

echo XBPS_ALLOW_RESTRICTED=yes > etc/conf

./xbps-src pkg discord

sudo xbps-install xtools

cd masterdir

xi discord

xbps-install libatomic


echo "These instructions were found here:  https://www.reddit.com/r/discordapp/comments/8b6nf3/well_it_looks_like_your_discord_installation_is/      and here:       https://www.reddit.com/r/voidlinux/comments/cl7cnf/installing_discord/"
